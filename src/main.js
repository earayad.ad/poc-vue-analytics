import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import './assets/main.css'

window.sendTagGoogleAnalytics = (event) => {
  window.dataLayer = window.dataLayer || [];
  window.dataLayer.push({
    event: event,
  });
};

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
